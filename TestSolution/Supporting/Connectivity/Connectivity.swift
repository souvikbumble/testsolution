//
//  Connectivity.swift
//  ORBIIT
//
//  Created by Souvik on 1/10/19.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
