//
//  ViewController.swift
//  TestSolution
//
//  Created by Souvik on 1/10/19.
//

import UIKit
import Toast_Swift
import Alamofire
import SDWebImage
import SVProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var mItemData : [Item]?
    @IBOutlet var mListTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generateAlert()
        self.mListTV.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
    }
    
    func generateAlert(){
        let alertController = UIAlertController(title: "Enter your Email", message: "", preferredStyle: .alert)
        
        alertController.addTextField {
            $0.placeholder = "Enter Email"
            $0.addTarget(alertController, action: #selector(alertController.textDidChangeInLoginAlert), for: .editingChanged)
        }
        let saveAction = UIAlertAction(title: "Get List", style: .default, handler: { alert -> Void in
            let emailTextField = alertController.textFields![0] as UITextField
            print("Email \(emailTextField.text!)")
            guard emailTextField.text != nil
                else { return } // Should never happen
            self.doSomething(email: emailTextField.text!)
        })
        
        
        saveAction.isEnabled = false
        alertController.addAction(saveAction)
        
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func updateEmailButton(_ sender: Any) {
        self.generateAlert()
    }
    
    func doSomething(email: String) {
        print("dsfdsfsdfdsfsfv")
        if Connectivity.isConnectedToInternet
        {
            
            self.getList(email: email)
            
        }else{
            self.view.makeToast("No Internet.")
        }
    }
    // MARK: - API CALL
    func getList(email : String) {
        SVProgressHUD.show()
        TESTSOLUTION_API_SERVICE.postListData(params: ["emailId": email as AnyObject]) { (result, error) in
            SVProgressHUD.dismiss()
            if let lResult = result {
                
                print(lResult)
                DispatchQueue.main.async(execute: {
                    self.mItemData = lResult.items
                    self.mListTV.reloadData()
                })
                
            }
            if let lError = error{
                SVProgressHUD.dismiss()
                print(lError.localizedDescription)
            }
        }
        
    }
    
    // MARK: - TableViewView Delegate & Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if(tableView == self.mListTV){
            if self.mItemData != nil {
                return (self.mItemData?.count)!
            }
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var defaultCell : UITableViewCell!
        if(tableView == self.mListTV )
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.mName.text = "\(mItemData![indexPath.row].firstName) \(mItemData![indexPath.row].lastName)"
            cell.mEmailLbl.text = mItemData![indexPath.row].emailID
            let url = String(format: "%@",self.mItemData![indexPath.row].imageURL)
            let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.mImge.sd_setImage(with: URL(string: urlString!), placeholderImage: UIImage(named: "user"))
            return cell
        }
        return defaultCell!
    }
    
}

extension UIAlertController {
    
    func isValidEmail(_ email: String) -> Bool {
        return email.characters.count > 0 && NSPredicate(format: "self matches %@", "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,64}").evaluate(with: email)
    }
    
    
    @objc func textDidChangeInLoginAlert() {
        if let email = textFields?[0].text,
            let action = actions.last {
            action.isEnabled = isValidEmail(email)
        }
    }
}

