//
//  ListModel.swift
//  TestSolution
//
//  Created by Souvik on 1/10/19.
//

import Foundation

struct ListData: Codable {
    let items: [Item]
}

struct Item: Codable {
    let emailID, lastName: String
    let imageURL: String
    let firstName: String
    
    enum CodingKeys: String, CodingKey {
        case emailID = "emailId"
        case lastName
        case imageURL = "imageUrl"
        case firstName
    }
}
