//
//  AuthRequest.swift
//  ExactMVC
//
//  Created by Souvik on 1/10/19.
//

import Foundation
import UIKit
import Alamofire

enum RestAPI {
    case listData
    
    
    
    static func service(for aServiceType: RestAPI) ->String {
        var path = ""
            switch aServiceType {
            case .listData:
                path = "list"
                return URLConstatns.BaseUrl + path
            
        }
        
    }
}

enum APIServiceMethod {
    case post
}

typealias APIServiceCompletionHandler = ((_ result: Any?, _ error: Error?) ->Void)

class APIService {
    static func perform(servideType aType: RestAPI,
                        method: APIServiceMethod,
                        params: [String: Any]?,
                        completion: @escaping APIServiceCompletionHandler) {
        let urlPath = RestAPI.service(for: aType)
        
        print(urlPath)
        guard let url = URL(string: urlPath) else {
            let error = NSError(domain: "apiservice.error", code: 991, userInfo: [NSLocalizedDescriptionKey: "Request url is invalid.", NSLocalizedFailureReasonErrorKey: "Invalid url path for '\(urlPath)' service type"])
            completion(nil, error)
            return
        }
        
        switch method {
        case .post:
            sendPostRequest(url: url, params: params, completion: completion)
        default:
            break
        }
    }
    
    
    
    static func sendPostRequest(url: URL,
                                params: [String: Any]?,
                                completion: @escaping APIServiceCompletionHandler) {
        let headers = ["Content-Type": "Application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .success(_):
                completion(response.result.value, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }

    
}
