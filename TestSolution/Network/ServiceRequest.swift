//
//  ServiceRequest.swift
//  ExactMVC
//
//  Created by Souvik on 1/10/19.
//


import Foundation
import Alamofire

class TESTSOLUTION_API_SERVICE: APIService {
    
    //MARK: -List Data
    static func postListData(params:[String: AnyObject]?,
                                         completion: @escaping (_ result: ListData?, _ error: Error?) -> Void) {
        perform(servideType: .listData, method: .post, params: params) { (result, error) in
            if let lError = error {
                completion(nil, lError)
                return
            }

            if let lResult = result{
                print(lResult)
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: lResult, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let decoded = try JSONDecoder().decode(ListData.self, from: jsonData)
                    completion(decoded, nil)
                }catch let encodeError{
                    print(encodeError)
                    completion(nil, encodeError)
                }
            }
        }
        
    }
  
}




